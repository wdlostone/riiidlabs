import requests
import time
from flask import Flask, request
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

# Create the API
class Riiid(Resource):
    def get(self, num):
        # Convert the time from hours to seconds to subtract from Unix time
        # inputTime = 6
        # take in the input time from the Curl
        inputTime = num
        # get the seconds to subtract from the unix time from the latest post
        subtractTime = inputTime * 3600

        # print testing but can stay in for viewing of incrementing
        print("This is the input time")
        # print(inputTime)

        # additional print testing
        # print("This is the time in seconds that will be subtracted from the curret Epic time")
        # print(subtractTime)

        # Get the latest ID Number
        latestItemURL = requests.get("https://hacker-news.firebaseio.com/v0/maxitem.json?print=pretty")
        latestItem = latestItemURL.json()

        # Get the URL with the latest ID
        latestItemURL = "https://hacker-news.firebaseio.com/v0/item/%s.json?print=pretty" % latestItem
        # print(latestItemURL)

        # Get the data from the latest ID
        responseLatestID = requests.get(latestItemURL)

        # Convert the latest post to JSON
        latestItemData = responseLatestID.json()
        
        # Get the Time from the latest ID
        # print("This is the latest time for the latest post")
        # print(latestItemData['time'])
        latestPostTime = (latestItemData['time'] - 3600)
        # print(latestPostTime)

        # print("This is the latest time minus the input hours")
        # print(latestItemData['time'] - subtractTime)
        endTime = latestItemData['time'] - subtractTime

        nextSmallestItem = latestItem
        # can use larger number for testing to actually get comments
        # nextSmallestItem = 24941390
        # newTime = latestItemData['time']
        # newTime = 1603921664
        # This will iterate through the item ids until the time from the latest ID is smaller that the endTime
        # while(newTime > endTime):
        currentCountHigh = 0

        while(latestPostTime > endTime):
        # If it hits the end of the while loop
            if latestPostTime == endTime:
                highestComment = newQuerryResponse
                return highestComment

            # generate the new URL that will increment
            newURL = "https://hacker-news.firebaseio.com/v0/item/%s.json?print=pretty" % nextSmallestItem
            # print(newURL)
            # make the request to the newURL
            newQuerry = requests.get(newURL)
            # make the response a JSON response
            newQuerryResponse = newQuerry.json()
            # get the time from the new response and set it to newTime which will be decremented

            # increment down from the last item
            nextSmallestItem -= 1
            # print("This is the next smallest item")
            # print(nextSmallestItem)
            # print("This is the next smallest time")
            # print(newTime)
            print(newQuerryResponse['id'])
            # I added a sleep due to rate limiting
            time.sleep(1)
            if "kids" in newQuerryResponse:
                kidsCount = len(newQuerryResponse['kids'])
                print(kidsCount)
                kidsCountNext = 0
                if(kidsCount > currentCountHigh):
                    highID = newQuerryResponse['id']
                    # kidsID = newQuerryResponse['kids']
                    currentCountHigh = kidsCount
                    # below is more print testing for local viewing
                    print("%s is the highest ID and %d is kids high" % (highID, kidsCount))
            # return{'result': num*10}

# Create the resource
api.add_resource(Riiid, '/riiid/<int:num>')

# run the app
if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
