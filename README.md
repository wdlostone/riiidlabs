# Riiid Labs

## Pipeline
This project is setup for a multi stage build
* Build
* Test

### Build
For this stage I am using Docker in Docker with Docker being the executor while using Gitlab Runners to build from the Dockerfile
Once completed I am then pusing the build to the local Gitlab registry. That way the image can be used for different environments

### Test
For testing I have the container tested against known CVEs with Klar analyzer. For this example to show that it's working I intentionally used a Docker image that had vulnerabilities. If you look at the build it shows that this build did not pass the container scanner.

Along with security scanning I also have it do a License scan.

Since I used Python as part of the testing process I had it do a dependency scan

## Deploy to production
As mentioned above I pushed it to the Gitlabs container registry. To deploy would be dependent on the environment that is used. For Kubernetes a deployment would need to be developed, or a Helm Chart if it was a big enough project

## How to deploy and test locally
The project is intended to be built as a container. To do that complete the following steps:

* Do a git clone of the project

* Build the container
docker build -t myimage .

* Run the image. Keep this terminal open and you will see it print out testing information
docker run -p 5001:5000 myimage

* In another terminal test the image. In the example below 3 represents h
curl http://127.0.0.1:5001/riiid/3
