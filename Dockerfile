FROM python:3.8
COPY requirements.txt requirements.txt
COPY ./app.py /tmp

RUN pip3 install -r requirements.txt

CMD python3 /tmp/app.py
